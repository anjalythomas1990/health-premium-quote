-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 15, 2017 at 02:19 PM
-- Server version: 10.1.24-MariaDB
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `anjaly`
--

-- --------------------------------------------------------

--
-- Table structure for table `health`
--

CREATE TABLE `health` (
  `p_id` int(50) NOT NULL,
  `p_name` varchar(100) DEFAULT NULL,
  `p_gender` varchar(50) DEFAULT NULL,
  `p_age` int(100) DEFAULT NULL,
  `p_hyper` varchar(50) DEFAULT NULL,
  `p_bp` varchar(50) DEFAULT NULL,
  `p_bsugar` varchar(50) DEFAULT NULL,
  `p_overwt` varchar(50) DEFAULT NULL,
  `p_smoke` varchar(50) DEFAULT NULL,
  `p_alco` varchar(50) DEFAULT NULL,
  `p_dalyexer` varchar(50) DEFAULT NULL,
  `p_drugs` varchar(50) DEFAULT NULL,
  `p_insur` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `health`
--

INSERT INTO `health` (`p_id`, `p_name`, `p_gender`, `p_age`, `p_hyper`, `p_bp`, `p_bsugar`, `p_overwt`, `p_smoke`, `p_alco`, `p_dalyexer`, `p_drugs`, `p_insur`) VALUES
(5, 'norman', 'Male', 34, 'NO', 'NO', 'NO', 'YES', 'NO', 'YES', 'YES', 'NO', '6855.981000000001'),
(6, 'norman', 'Male', 25, 'NO', 'NO', 'NO', 'YES', 'NO', 'YES', 'YES', 'NO', '6232.71');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `health`
--
ALTER TABLE `health`
  ADD PRIMARY KEY (`p_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `health`
--
ALTER TABLE `health`
  MODIFY `p_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
